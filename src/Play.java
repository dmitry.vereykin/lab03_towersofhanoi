/**
 * Created by Dmitry Vereykin on 9/24/2015.
 * With assistance from: Howard Fulmer, Heather Myers
 */
import java.util.*;

public class Play {

    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        int numRings = 0;
        int count = 0;
        String confirm = "";
        boolean confirmed = false;
        int startPeg;
        int endPeg;
        System.out.println("There is no maximum limit, but a game with 64 rings will take at least 590 billion years to complete.");

        while(!confirmed) {
            System.out.println("Enter number of rings: ");
            numRings = keyboard.nextInt();

            if (numRings > 10) {
                System.out.println("This game can take a lot of time, are you sure? y/n");
                keyboard.nextLine();
                confirm = keyboard.nextLine();

                    if (confirm.equalsIgnoreCase("y")) {
                        System.out.println("You've been warned.");
                        confirmed = true;
                    } else if (confirm.equalsIgnoreCase("n")) {
                        confirmed = false;
                    }

            } else {
                confirmed = true;
            }
        }

        System.out.println("\nMove all rings to the third peg.\n");
        Towers game = new Towers(numRings);
        game.printPegs();

        while (!game.victoryCondition()) {
            System.out.print("From?");
            startPeg = keyboard.nextInt();

            endPeg = keyboard.nextInt();

            if (game.validMove(startPeg, endPeg)) {
                game.move(startPeg,endPeg);
                game.printPegs();
                count++;
            } else {
                System.out.println("Invalid move!");
            }
        }

        System.out.println("The game was finished in " + count + " moves.");
    }
}
